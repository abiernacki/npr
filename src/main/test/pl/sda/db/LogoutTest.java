package pl.sda.db;

import org.junit.Test;
import pl.sda.Logout;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LogoutTest {

    @Test
    public void testDoGet() throws IOException, ServletException {
        // given
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        HttpSession mockSession = mock(HttpSession.class);
        RequestDispatcher mockDispatcher = mock(RequestDispatcher.class);
        String indexPage = "index.jsp";
        when(mockRequest.getRequestDispatcher(indexPage)).thenReturn(mockDispatcher);
        when(mockRequest.getSession()).thenReturn(mockSession);

        // when
        Logout logout = new Logout();
        logout.doGet(mockRequest, mockResponse);

        // then
        verify(mockSession).invalidate();
        verify(mockRequest).getRequestDispatcher(indexPage);
        verify(mockDispatcher).forward(mockRequest,mockResponse);
    }
}
