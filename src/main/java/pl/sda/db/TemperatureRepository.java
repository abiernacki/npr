package pl.sda.db;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import org.jooq.Result;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.Temperature;
import pl.sda.db.data.generated.tables.Users;
import pl.sda.db.data.generated.tables.records.TemperatureRecord;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;


public class TemperatureRepository extends DatabaseRepository {

    public void deleteAll() {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Temperature temperature = Tables.TEMPERATURE;
            ctx.delete(temperature).execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Temperature temperature = Tables.TEMPERATURE;
            ctx.delete(temperature).where(temperature.ID.equal(id));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer save(TemperatureRecord record) {
        if (Strings.isNullOrEmpty(String.valueOf(record.getDate()))
                || Strings.isNullOrEmpty(String.valueOf(record.getDayCounter()))
                || Strings.isNullOrEmpty(String.valueOf(record.getTemperature())))
                 {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        return store(record);
    }

    public List<TemperatureRecord> getTemperature(Integer userId) {
        // wykonanie zapytania
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Temperature temperature = Tables.TEMPERATURE;
            return ctx.selectFrom(temperature)
                    .where(temperature.USER_ID.equal(userId))
                    .fetch().stream().collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public TemperatureRecord getById(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Temperature temperature = Tables.TEMPERATURE;
            return ctx.selectFrom(temperature)
                    .where(temperature.ID.equal(id))
                    .fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public TemperatureRecord getTemperatureByDayCounter(Integer daycounter, Integer userId) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Temperature temperature = Tables.TEMPERATURE;
            return ctx.selectFrom(temperature).where(temperature.DAY_COUNTER.equal(daycounter))
                    .and(temperature.USER_ID.equal(userId)).fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
