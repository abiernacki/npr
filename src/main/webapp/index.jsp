<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Portal a'la Wiki zrealizowany przez grupę SDA_Team1"/>
    <meta name="keywords" content="ala wiki"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="style1.css">
    <link rel="stylesheet" href="style3.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <c:set var="pageName" value="index"/>
    <c:set var="isLogin" value="<%=request.getSession().getAttribute("isLogin")%>"/>
    <jsp:include page="menu.jsp">
        <jsp:param name="pageName" value="${pageName}"/>
    </jsp:include>

    <div class="jumbotron">
        <h1 class="display-3"><strong>Naturalne Planowanie Rodziny</strong></h1>
        <p class="lead"> Witajcie, aplikacja ułatwia zaplanowanie rodziny, dostęp do informacji online zasze i wszędzie. </p>
        <c:if test="${isLogin eq 'true'}">
            <a href="editpost.jsp">
                <button type="button" class="btn btn-warning">Dodaj artykuł</button>
            </a>
        </c:if>
        <c:if test="${empty isLogin}">
            <a href="register.jsp">
                <p><a class="btn btn-lg btn-success" href="register.jsp" role="button">Dołącz już teraz!</a></p>
            </a>
        </c:if>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="https://cdn.pixabay.com/photo/2015/03/30/20/33/heart-700141_960_720.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://cdn.pixabay.com/photo/2012/03/04/01/01/baby-22194_960_720.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                <img class="d-block w-100" src="https://cdn.pixabay.com/photo/2014/09/23/06/04/brothers-457237_960_720.jpg" alt="Third slide">
            </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://pixabay.com/get/ed37b80e2dfc1c22d9584518a3454f91e07fe6d104b0144290f6c77ca0eebd/mother-429158_1920.jpg" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://pixabay.com/get/ee3cb50a2ee9002ad25a5840ef454195eb73e6c818b4124493f6c471a2ec/fog-79456_1920.jpg" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>


    <div class="row marketing">

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
        </div>

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
        </div>

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
        </div>

        <c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 15)%>
        </c:set>
        <div class="col-lg-6">
        </div>
    </div>
    <jsp:include page="footer.jsp"/>
</div> <!-- /container -->
</body>
</html>
