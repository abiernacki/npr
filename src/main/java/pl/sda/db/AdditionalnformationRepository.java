package pl.sda.db;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.AdditionalInformation;
import pl.sda.db.data.generated.tables.Mucus;
import pl.sda.db.data.generated.tables.records.AdditionalInformationRecord;
import pl.sda.db.data.generated.tables.records.MucusRecord;

import java.sql.Connection;
import java.sql.SQLException;

public class AdditionalnformationRepository extends DatabaseRepository {

    public Integer save(AdditionalInformationRecord record) {
        if (Strings.isNullOrEmpty(String.valueOf(record.getOvulatoryPain()))
                || Strings.isNullOrEmpty(String.valueOf(record.getTensionInTheBreast()))
                || Strings.isNullOrEmpty(String.valueOf(record.getNoise()))
                || Strings.isNullOrEmpty(String.valueOf(record.getFertileDays()))
                || Strings.isNullOrEmpty(String.valueOf(record.getCohabitation())))

        {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        return store(record);
    }

    public void deleteAll() {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            AdditionalInformation additionalInformation = Tables.ADDITIONAL_INFORMATION;
            ctx.delete(additionalInformation).execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            AdditionalInformation additionalInformation = Tables.ADDITIONAL_INFORMATION;
            ctx.delete(additionalInformation).where(additionalInformation.ID.equal(id));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public AdditionalInformationRecord getById(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            AdditionalInformation additionalInformation = Tables.ADDITIONAL_INFORMATION;
            return ctx.selectFrom(additionalInformation)
                    .where(additionalInformation.ID.equal(id))
                    .fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
