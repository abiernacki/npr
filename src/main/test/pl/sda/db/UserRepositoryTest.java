package pl.sda.db;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.db.data.generated.tables.records.UsersRecord;

import java.util.List;

public class UserRepositoryTest {

    @Test
    public void test1() {
        // given
        UserRepository db = new UserRepository();

        // when
        List<UsersRecord> users = db.getUsers();

        // then
        Assert.assertNotNull(users);
    }

    @Test
    public void testGetByAccessLevel() {
        // given
        UserRepository db = new UserRepository();
        String level = "admin";

        // when
        List<UsersRecord> users = db.getByAccessLevel(level);

        // then
        Assert.assertNotNull(users);
    }

    @Test
    public void testGetUserByEmail() {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();
        record.setUtcoffset("testO");
        record.setFirstname("testFirstName");
        record.setLastname("testLastName");
        String email = "testEmail";
        record.setEmail(email);
        record.setLogin("testLogin");
        record.setPassword("testPassword");
        Integer generatedId = repository.insert(record);

        // when
        UsersRecord result = repository.getUserByEmail(email);

        // then
        Assert.assertEquals(email, result.getEmail());

        //clean
        repository.delete(generatedId);
    }

    @Test
    public void testGetById() {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();
        record.setUtcoffset("testO");
        record.setFirstname("testFirstName");
        record.setLastname("testLastName");
        record.setEmail("testEmail");
        record.setLogin("testLogin");
        record.setPassword("testPassword");
        Integer generatedId = repository.insert(record);

        // when
        UsersRecord result = repository.getById(generatedId);

        // then
        Assert.assertNotNull(result);

        // clean
        repository.delete(generatedId);
    }

    @Test
    public void testInsert() {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();
        record.setUtcoffset("testO");
        record.setFirstname("testFirstName");
        record.setLastname("testLastName");
        record.setEmail("testEmail");
        record.setLogin("testLogin");
        record.setPassword("testPassword");

        // when
        Integer generatedId = repository.insert(record);

        // then
        Assert.assertNotNull(repository.getById(generatedId));

        // clean
        repository.delete(generatedId);
    }

    @Test
    public void testDelete() {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();
        record.setUtcoffset("testO");
        record.setFirstname("testFirstName");
        record.setLastname("testLastName");
        record.setEmail("testEmail");
        record.setLogin("testLogin");
        record.setPassword("testPassword");
        Integer generatedId = repository.insert(record);

        // when
        repository.delete(generatedId);

        // then
        Assert.assertNull(repository.getById(generatedId));

    }

    @Test
    public void testUpdate() {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();
        record.setUtcoffset("testO");
        record.setFirstname("testFirstName");
        record.setLastname("testLastName");
        record.setEmail("testEmail");
        record.setLogin("testLogin");
        record.setPassword("testPassword");
        Integer generatedId = repository.insert(record);
        String change = "CHANGED FIRST NAME";

        //when
//        record.setId(generatedId);
        record = repository.getById(generatedId);
        record.setFirstname(change);
        repository.update(record);

        //then
        Assert.assertEquals(change, repository.getById(generatedId).getFirstname());

        //clean
        repository.delete(generatedId);
    }

    @Test
    public void testUpdate2() {
        // given
        UserRepository repository = new UserRepository();
        UsersRecord record = new UsersRecord();
        record.setUtcoffset("testO");
        record.setFirstname("testFirstName");
        record.setLastname("testLastName");
        record.setEmail("testEmail");
        record.setLogin("testLogin");
        record.setPassword("testPassword");
        Integer generatedId = repository.insert(record);

        //when
        UsersRecord byId = repository.getById(generatedId);
        String change = "CHANGED FIRST NAME1";
        byId.setFirstname(change);
        repository.update2(byId);

        //then
        Assert.assertEquals(change, repository.getById(generatedId).getFirstname());

        //clean
        repository.delete(generatedId);
    }

}