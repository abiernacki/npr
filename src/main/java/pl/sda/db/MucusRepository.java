package pl.sda.db;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.Mucus;
import pl.sda.db.data.generated.tables.ObservationCard;
import pl.sda.db.data.generated.tables.records.MucusRecord;
import pl.sda.db.data.generated.tables.records.ObservationCardRecord;

import java.sql.Connection;
import java.sql.SQLException;

public class MucusRepository extends DatabaseRepository {

    public Integer save(MucusRecord record) {
        if (Strings.isNullOrEmpty(String.valueOf(record.getFeeling()))
                || Strings.isNullOrEmpty(String.valueOf(record.getLook()))
                || Strings.isNullOrEmpty(String.valueOf(record.getDate()))
                || Strings.isNullOrEmpty(String.valueOf(record.getDayCounter())))

        {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        return store(record);
    }

    public void deleteAll() {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Mucus mucus = Tables.MUCUS;
            ctx.delete(mucus).execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Mucus mucus = Tables.MUCUS;
            ctx.delete(mucus).where(mucus.ID.equal(id));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public MucusRecord getById(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            Mucus mucus = Tables.MUCUS;
            return ctx.selectFrom(mucus)
                    .where(mucus.ID.equal(id))
                    .fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
