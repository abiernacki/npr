<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<c:set var="pageName" value="<%=request.getParameter("pageName")%>"/>

<c:set var="isLogin" value="<%=request.getSession().getAttribute("isLogin")%>"/>
<c:set var="firstName" value="<%=request.getSession().getAttribute("firstName")%>"/>
<c:set var="accessLevel" value="<%=request.getSession().getAttribute("accessLevel")%>"/>

<%--Diagnostyka--%>
<%--<c:out value="GET: ${pageName}"></c:out>--%>

<div class="header clearfix">
    <nav>
        <ul class="nav nav-pills nav-fill">
            <li class="nav-item">
                <c:if test="${isLogin eq 'true'}">
                <h3 class="text-muted">Witaj: ${firstName}! <a href="<%= application.getContextPath() %>/logout">
                    <button class="przycisk" type="button">Wyloguj</button>
                </a>

                    <c:if test="${accessLevel eq 'admin'}">
                    <a href="adminpage.jsp">
                        <button type="button" class="btn btn-danger">Admin</button>
                    </a>
                    </c:if>
                    </c:if>
                    <c:if test="${empty isLogin}">
                        <a class="nav-link"href="login.jsp">
                            Zaloguj
                        </a>
                    </c:if>
            </li>
            <c:if test="${isLogin eq 'true'}">
            <li class="nav-item">
                <a href="#">Pozycja</a>
            </li>
            </c:if>
            <li class="nav-item">
                <c:if test="${pageName ne 'index'}"><a class="nav-link" href="index.jsp"></c:if>
                <c:if test="${pageName eq 'index'}"><a class="nav-link active" href="index.jsp"></c:if>Początek<span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <c:if test="${pageName ne 'about'}"> <a class="nav-link" href="about.jsp"></c:if>
                <c:if test="${pageName eq 'about'}"> <a class="nav-link active" href="about.jsp"> </c:if>O mnie</a>
            </li>
            <li class="nav-item">
                <c:if test="${pageName ne 'contact'}"><a class="nav-link" href="contact.jsp"></c:if>
                <c:if test="${pageName eq 'contact'}"><a class="nav-link active" href="contact.jsp"></c:if>Kontakt</a>
            </li>
        </ul>
    </nav>
</div>