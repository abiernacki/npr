package pl.sda.db;

import org.junit.Assert;
import org.junit.Test;
import pl.sda.db.data.generated.tables.records.TemperatureRecord;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class TemperatureRepositoryTest {

    @Test
    public void testStore() {
        TemperatureRepository temperatureRepository = new TemperatureRepository();
        temperatureRepository.deleteAll();

        TemperatureRecord temperatureRecord1 = new TemperatureRecord();
        temperatureRecord1.setUserId(2);
        temperatureRecord1.setDate(new Timestamp(new Date().getTime()));
        temperatureRecord1.setDayCounter(1);
        temperatureRecord1.setTemperature(36.6);
        temperatureRepository.save(temperatureRecord1);

        TemperatureRecord temperatureRecord2 = new TemperatureRecord();
        temperatureRecord2.setUserId(4);
        temperatureRecord2.setDate(new Timestamp(new Date().getTime()));
        temperatureRecord2.setDayCounter(1);
        temperatureRecord2.setTemperature(36.6);
        temperatureRepository.save(temperatureRecord2);


        List<TemperatureRecord> temperatureRecordList = temperatureRepository.getTemperature(2);
        Assert.assertEquals(1, temperatureRecordList.size());
        TemperatureRecord result = temperatureRecordList.get(0);

        Assert.assertEquals(temperatureRecord1.getDate(), result.getDate());
        Assert.assertEquals(temperatureRecord1.getUserId(), result.getUserId());
        Assert.assertEquals(temperatureRecord1.getDayCounter(), result.getDayCounter());
        Assert.assertEquals(temperatureRecord1.getTemperature(), result.getTemperature());
    }

    @Test
    public void getByIdTest() {

        TemperatureRepository temperatureRepository = new TemperatureRepository();

        TemperatureRecord temperatureRecord = new TemperatureRecord();
        temperatureRecord.setUserId(2);
        temperatureRecord.setDate(new Timestamp(new Date().getTime()));
        temperatureRecord.setDayCounter(1);
        temperatureRecord.setTemperature(36.6);
        temperatureRepository.save(temperatureRecord);

        Integer id = temperatureRecord.getId();
        Assert.assertNotNull(id);

        TemperatureRecord byId = temperatureRepository.getById(id);
        Assert.assertNotNull(byId);
        Assert.assertEquals(temperatureRecord, byId);
    }

}