package pl.sda.db;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import pl.sda.db.data.generated.Tables;
import pl.sda.db.data.generated.tables.ObservationCard;
import pl.sda.db.data.generated.tables.records.ObservationCardRecord;
import pl.sda.db.data.generated.tables.records.TemperatureRecord;

import java.sql.Connection;
import java.sql.SQLException;

public class ObservationCardRepository extends DatabaseRepository {

    public Integer save(ObservationCardRecord record) {
        if (Strings.isNullOrEmpty(String.valueOf(record.getCycleNumber()))
                || Strings.isNullOrEmpty(String.valueOf(record.getCardNumber()))
                || Strings.isNullOrEmpty(String.valueOf(record.getTheShortestCycle_21_2f20()))
                || Strings.isNullOrEmpty(String.valueOf(record.getFirstDayOfMucusOccurence()))
                || Strings.isNullOrEmpty(String.valueOf(record.getDayOfFertileMucus()))
                || Strings.isNullOrEmpty(String.valueOf(record.getRushDay()))
                || Strings.isNullOrEmpty(String.valueOf(record.getRushDay_3()))
                || Strings.isNullOrEmpty(String.valueOf(record.getThirdDayAboveTheLine()))
                || Strings.isNullOrEmpty(String.valueOf(record.getLengthOfTh_LutealPhase()))
                || Strings.isNullOrEmpty(String.valueOf(record.getCycleLength()))
                || Strings.isNullOrEmpty(String.valueOf(record.getTheLongestOfTheLastCycles()))
                || Strings.isNullOrEmpty(String.valueOf(record.getTheShortestOfTheLastCycles()))
                || Strings.isNullOrEmpty(String.valueOf(record.getHourOfTemperatureMeasurement()))
                || Strings.isNullOrEmpty(String.valueOf(record.getTheLastDayOfMenstruation())))

        {
            throw new IllegalArgumentException("Podane dane są niepoprawne.");
        }

        return store(record);
    }

    public void deleteAll() {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            ObservationCard observationCard = Tables.OBSERVATION_CARD;
            ctx.delete(observationCard).execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            ObservationCard observationCard = Tables.OBSERVATION_CARD;
            ctx.delete(observationCard).where(observationCard.ID.equal(id));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ObservationCardRecord getById(Integer id) {
        try (Connection conn = connection()) {
            DSLContext ctx = jooq(conn);
            ObservationCard observationCard = Tables.OBSERVATION_CARD;
            return ctx.selectFrom(observationCard)
                    .where(observationCard.ID.equal(id))
                    .fetchOne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
